import java.io.*;
class ArrayDemo{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]=new int [5];
		System.out.println("Enter array elements");
		for(int i=0;i<5;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int sum=0;
		for(int i=0;i<5;i++){
			sum=sum+arr[i];
		}
		System.out.println("Sum="+sum);
	}
}
	
