class CompareToIgnoreCaseDemo{
	public static void main(String[]args){
		String str1="SHRUTI";
		String str2="shrUti";
		String str3="shru";

		System.out.println(str1.compareToIgnoreCase(str2));
		System.out.println(str1.compareToIgnoreCase(str3));

	}
}
