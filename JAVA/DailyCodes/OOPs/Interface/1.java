interface Demo{
	void gun();
	void fun();
}

class DemoChild implements Demo{

       public void fun(){
		
	       System.out.println("in fun");
	}

	public void gun(){

		System.out.println("in gun");
	}
}

class Client{
	public static void main(String[]args){
		DemoChild obj=new DemoChild();
		obj.fun();
		obj.gun();
	}
}
