class Parent{
        void fun(){     // access specifier: default

        }
}

class Child extends Parent{
       private void fun(){         // access specifier: private

        }
}
 
