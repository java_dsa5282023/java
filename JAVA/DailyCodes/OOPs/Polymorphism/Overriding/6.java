//Overriding scenario: primitive datatype 


class Parent{
	int fun(){
		System.out.println("Parent fun");
		return 10;
	}
}

class Child extends Parent{
	char fun(){
		System.out.println("Child fun");
		return 'a';
	}
}

class Client{
	public static void main(String[]args){
		Parent p=new Child();
		p.fun();
	}
}
