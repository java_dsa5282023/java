class Parent{
	Parent(){
		System.out.println("Parent constructor");
	}

	void property(){
		System.out.println("home,car,gold");
	}

	void marry(){
		System.out.println("Deepika Padukon");
	}
}

class Child extends Parent{
	Child(){
		System.out.println("Child constructor");
	}

	void marry(){
		System.out.println("NA");
	}
}


class Client{
	public static void main(String[]args){
		Child obj=new Child();
		obj.property();
		obj.marry();
	}
}
