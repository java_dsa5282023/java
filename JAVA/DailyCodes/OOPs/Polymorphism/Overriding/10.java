class Parent{
        private void fun(){	// access specifier: private 
                 System.out.println("Parent fun");
        }
}

class Child extends Parent{
        void fun(){         // access specifier: default
                System.out.println("Child fun");
        }
}

class Client{
	public static void main(String[]args){
		Parent obj=new Child();
		obj.fun();
	}
}
