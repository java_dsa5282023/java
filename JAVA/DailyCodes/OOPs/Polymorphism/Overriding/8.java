class Parent{
	public void fun(){     // access specifier: public 

	}
}

class Child extends Parent{
	void fun(){         // access specifier: default
	
	}
}

