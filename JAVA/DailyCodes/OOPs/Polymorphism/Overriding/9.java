class Parent{
        void fun(){     // access specifier: default 

        }
}

class Child extends Parent{
       public  void fun(){         // access specifier: public

        }
}

