// overloading scenarios

class Demo{
	void fun(int x,float y){
		System.out.println("Int-float parameters");
	}
	void fun(float x,int y){
		System.out.println("float-int parameters");
	}
}

class Client{
	public static void main(String[]args){
		Demo obj=new Demo();
		obj.fun(10,10);
	}
}

