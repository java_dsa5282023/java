//Real time eg.
class Movie{
	String mName="Kantara";
	static int ticketPrice=400;

	void mInfo(){
		System.out.println(mName);
		System.out.println(ticketPrice);
	}
}

class MainDemo{
	public static void main(String[]args){
		Movie obj1=new Movie();
		Movie obj2=new Movie();

		obj1.mInfo();
		obj2.mInfo();

		System.out.println();

		obj2.mName="DDLJ";
		obj2.ticketPrice=111;

		obj1.mInfo();
                obj2.mInfo();

	}
}

