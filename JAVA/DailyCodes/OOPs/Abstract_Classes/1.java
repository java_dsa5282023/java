// abstract class: real time example

abstract class Parent{
	void career(){
		System.out.println("Enginner");
	}

	 abstract void marry();
}


class Child extends Parent{
	void marry(){
		System.out.println("Kriti");
	}
}


class Client{
	public static void main(String[]args){
		Child obj=new Child();
		obj.career();
		obj.marry();
	}
}
