abstract class University{
	void U_name(){
		System.out.println("SPPU");
	}
	abstract void collegeName();
}

class College extends University{
	void collegeName(){
		System.out.println("JSPM NTC");
	}
}

class Client{
	public static void main(String[]args){
		University obj=new College();
                obj.U_name();
                obj.collegeName();
	}
}

