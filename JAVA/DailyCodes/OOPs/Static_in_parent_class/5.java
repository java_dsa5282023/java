class Parent{

	Parent(){
		System.out.println("Parent constructor");

	}

	void method1(){

		System.out.println("Parent method1");

	}

}

class Child extends Parent{
	Child(){
		super.method1();
		System.out.println("Child constructor");

	}

	void method1(){

		System.out.println("Child method1");
	}

}

class Client {
	public static void main(String[]args){
		Child obj=new Child();
		obj.method1();

	}
}


