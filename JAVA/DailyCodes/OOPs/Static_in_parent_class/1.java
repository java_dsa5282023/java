class Parent{
	static{
		System.out.println("Parent static block");
	}
}

class Child extends Parent{
	static{
		System.out.println("Child static block");
	}
}

class Client{
	public static void main(String[]args){
		Parent obj=new Child();

	}
}
