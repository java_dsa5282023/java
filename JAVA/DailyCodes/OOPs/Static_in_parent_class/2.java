class Parent{
	static int x=10;
	static {
		System.out.println("Parent static block");
	}
	static void access(){
		System.out.println(x);
	}
}

class Child extends Parent{

	//super();
	static {
		System.out.println("Child static block");
		System.out.println(x);
		access();
	}
}

class Client{
	public static void main(String[]args){
		System.out.println("In main");
		Parent obj=new Child();
	}
}
