import java.util.*;
class Demo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num:");
		int num=sc.nextInt();

		int sum=0;
		for(int i=1;i<=num;i++){
			if(i%3!=0){
				sum=sum+i;
			}
		}
		System.out.println("Sum of no.not divisible by 3 = "+sum);
	}
}
