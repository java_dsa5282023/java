import java.io.*;
class ArrayDemo{
        public static void main(String [] args) throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

		int min=arr[0];

		for(int i=0;i<size;i++){
			if(arr[i]<min){
				min=arr[i];
			}
		}

		System.out.println("Minimum element in array:"+min);
	}
}
