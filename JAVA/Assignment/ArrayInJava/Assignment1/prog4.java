import java.io.*;
class ArrayDemo{
        public static void main(String [] args) throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

		System.out.println("Enter element to search:");
		int ele=Integer.parseInt(br.readLine());

		for(int i=0;i<size;i++){
			if(arr[i]==ele){
				System.out.println("Index of element="+i);
			}
		}
	}
}

