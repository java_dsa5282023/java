import java.io.*;
class ArrayDemo{
        public static void main(String [] args) throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

		int count1=0,count2=0;

		for(int i=0;i<size;i++){
			if(arr[i]%2==0){
				count1++;
			}else{
				count2++;
			}
		}
		System.out.println("Even no. count="+count1);
		System.out.println("odd no. count="+count2);
	}
}

