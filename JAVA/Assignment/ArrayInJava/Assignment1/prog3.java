import java.io.*;
class ArrayDemo{
        public static void main(String [] args) throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                int sum1=0,sum2=0;

                for(int i=0;i<size;i++){
                        if(arr[i]%2==0){
                               sum1=sum1+arr[i];
                        }else{
                                sum2=sum2+arr[i];
                        }
                }
                System.out.println("Even no. sum="+sum1);
                System.out.println("odd no. sum="+sum2);
        }
}

