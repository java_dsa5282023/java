import java.io.*;
class ArrayDemo{
        public static void main(String[] rgs)throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		System.out.println();

		for(int i=0;i<size;i++){
			int sum=0,num=arr[i],rev=0;

			while(num!=0){
				int rem=num%10;
				rev=rev*10+rem;
				num=num/10;
			}
			if(rev==arr[i]){
				System.out.println("Pallindrome number "+rev+" found at "+i);
			}
		}
	}
}

