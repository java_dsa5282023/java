import java.io.*;
class ArrayDemo{
	public static void main(String[] rgs)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("Enter array elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<size;i++){
			int sum=0;
			for(int j=0;j<arr[i];j++){
				if(arr[i]%j==0){
					sum=sum+j;
				}
			}
			if(sum==arr[i]){
				System.out.println("Perfect no "+ arr[i]+" found at "+i);
			}
		}
	}
}
