import java.io.*;
class ArrayDemo{
        public static void main(String[] rgs)throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

		for(int i=0;i<size;i++){
			int num=arr[i];
			int sum=0;
			while(num!=0){
				int rem=num%10;
				int fact=1;

				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
				sum=sum+fact;
				num=num/10;
			}
			if(sum==arr[i]){
				System.out.println("Strong no "+arr[i]+" found at "+i);
			}
		}
	}
}

