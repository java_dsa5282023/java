import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
                 
	   
		for(int i=0;i<size;i++){
			int count=0;
			int temp=arr[i];

			for(int j=1;j<=temp;j++){
				if(temp%j==0){
					count++;
				}
			}
			
			if(count>=3){
				System.out.println(temp+" found at index "+i);
		        }
         	
		}
	}
}
