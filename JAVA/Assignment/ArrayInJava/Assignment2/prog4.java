import java.io.*;
class ArrayDemo{
        public static void main(String [] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                
                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());
                
                char arr[]=new char[size];
                
                for(int i=0;i<size;i++){
                        arr[i]=(char)br.read();
			br.skip(1);
                }    

		for(int i=0;i<size;i++){
			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'){
				System.out.println(arr[i]);
			}
		}
	}
}
