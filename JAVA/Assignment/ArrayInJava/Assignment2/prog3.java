import java.io.*;
class ArrayDemo{
        public static void main(String [] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                
                System.out.println("Enter array size:");
                int size=Integer.parseInt(br.readLine());
                
                int arr[]=new int[size];
                
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }    

		int prod=1;

		for(int i=0;i<size;i++){  
			if(i%2==1){
				prod=prod*arr[i];
			}
		}
		System.out.println("Product of odd index elements="+prod);
	}
}

