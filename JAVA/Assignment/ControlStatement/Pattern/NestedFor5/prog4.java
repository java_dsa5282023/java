import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number");
		int num=Integer.parseInt(br.readLine());

		int sum=0;

		while(num!=0){
			int rem=0;
			rem=num%10;
			num=num/10;
			int fact=1;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}
			sum=sum+fact;
		}
		System.out.println("Addition="+sum);
	}
}
