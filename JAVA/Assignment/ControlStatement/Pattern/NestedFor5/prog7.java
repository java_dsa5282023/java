import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter row:");
		int row=Integer.parseInt(br.readLine());

		char ch=(char)((65+(row*row+1)/2)+1);
		int num=((row*row+1)/2)+2;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(ch+" ");
				}else{
					System.out.print(num+" ");
				}
				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
