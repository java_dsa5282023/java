import java.io.*;
class Demo{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter char1:");
		char ch1=(char)br.read();
		br.skip(1);

		System.out.println("Enter char2:");
		char ch2=(char)br.read();

		if(ch1==ch2){
			System.out.println(ch1+" "+ch2);
		}else{
			int diff=ch1-ch2;
			System.out.println(diff);
		}
	}
}


