import java.io.*;
class Demo{
	public static void main(String [] args) throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row:");
		int row=Integer.parseInt(br.readLine());

		int a=row-row;
		int b=a+1;
		int c;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				c=a;
				System.out.print(c+" ");
				a=b;
				b=c+b;
			}
			System.out.println();
		}
	}
}
