class Demo{
	public static void main(String[]args){
		char ch='F';
		int num=1;
		int row=6;

		for(int i=1;i<=row;i++){
			char ch1=ch;
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(ch1+" ");
					ch1++;
				}else{
					System.out.print(num+" ");
					num++;
				}
			}
			ch--;
			System.out.println();
		}
	}
}
